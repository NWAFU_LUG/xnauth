#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
#   Author  :   renyuneyun
#   E-mail  :   renyuneyun@gmail.com
#   Date    :   16/01/02 17:51:20
#   Desc    :   
#   License :   LGPL V2.1 (See LICENSE)
#
import base64
from urllib.parse import unquote
import json
import math
import time
from config import DEBUG

def dp(text):
    if DEBUG > 0:
        print("------\n%s\n======" % text)
def dp1(text):
    if DEBUG > 1:
        print("------\n%s\n======" % text)

def decorate(data):
    '''
    decorate raw data of base64
    '''
    length = len(data)
    alength = math.ceil(length / 4.0) * 4
    data += "=" * (alength-length)
    return data

def decode(ticket):
    '''
    decode base64-ed json (ticket) to python dict
    '''
    ticket = decorate(ticket)
    json_str = unquote(base64.decodebytes(ticket.encode('utf8')).decode('utf8'))
    return json.loads(json_str)

def now():
    curtime = str(time.time()).split('.')
    return curtime[0] + curtime[1][0:3]

