#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
#   Author  :   renyuneyun
#   E-mail  :   renyuneyun@gmail.com
#   Date    :   15/12/30 12:10:51
#
from http.client import HTTPConnection
from urllib.parse import quote
from time import sleep
from signal import signal, SIGINT
from threading import Lock

import utils
from utils import DEBUG, dp, dp1
from config import config as cfg, urls

class ReturnedException(Exception):
    def __init__(self, info):
        self.info = info
        Exception.__init__(self)

def getCookie(connection, urls, headers):
    dp("getting cookie")
    url = urls['portal']
    iheaders = {}
    iheaders.update(headers)
    connection.request('GET', url, headers=iheaders)
    with connection.getresponse() as response:
        icookie = response.getheader('Set-Cookie')
        if icookie != None:
            cookie = icookie
        webpage_url = response.getheader('Location')# '/portal/templatePage/20150510015825585/login_custom.jsp'
        urls['wp:login_custom'] = webpage_url
        data = response.read()
    connection.request('GET', webpage_url, headers=iheaders)
    with connection.getresponse() as response:
        icookie = response.getheader('Set-Cookie') #seems to be identical to the former one
        if icookie != None:
            cookie = icookie
        data = response.read()
    cookie = cookie.split(';')[0]
    return cookie

def login(cookie, infos):
    url = urls['pws:li']
    iheaders = {
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'X-Requested-With': 'XMLHttpRequest',
            'Referer': urls['base'] + urls['wp:login_custom'],
            'Cookie': cookie,
            'Connection': 'keep-alive',
            }
    iuserdata = "id_userName=%s&userName=%s&id_userPwd=%s&userPwd=%s" % (cfg['id_userName'],cfg['userName'],cfg['id_userPwd'],cfg['userPwd'])
    iuserdata += '&serviceTypeHIDE=&serviceType=&userurl=&userip=&basip='
    iuserdata += '&language=%s&usermac=null&wlannasid=&entrance=null' % (infos['clientLanguage'])
    iuserdata += "&portalProxyIP=" + cfg['host'] + "&portalProxyPort=50200"
    iuserdata += '&dcPwdNeedEncrypt=%s&assignIpType=%s' % (infos['iNodePwdNeedEncrypt'], infos['assignIpType'])
    iuserdata += "&appRootUrl=" + quote(urls['appRoot'])
    iuserdata += '&manualUrl=&manualUrlEncryptKey='
    ibody = iuserdata
    return 'POST', url, iheaders, ibody

def afterLogin(cookie, infos):
    url = urls['tmpl:afterLogin'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg'], infos['byodServerIp'], infos['byodServerHttpPort'])
    iheaders = {
            'Cookie': cookie,
            'Connection': 'keep-alive',
            'Referer': urls['base'] + urls['wp:login_custom'],
            }
    return 'GET', url, iheaders, None

def openPage__online(cookie, infos):
    url = urls['tmpl:online'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg'])
    iheaders = {
            'Cookie': cookie,
            'Connection': 'keep-alive',
            'Referer': urls['tmpl:afterLogin'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg'], infos['byodServerIp'], infos['byodServerHttpPort']),
            }
    return 'GET', url, iheaders, None

def listenClose(cookie, infos):
    url = urls['tmpl:listenClose'] % infos['portalLink']
    iheaders = {
            'Cookie': cookie,
            'Connection': 'keep-alive',
            'Referer': urls['tmpl:online'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg']),
            }
    return 'GET', url, iheaders, None

def online_showTimer(cookie, infos):
    url = urls['tmpl:online_showTimer'] % (infos['portalLink'], utils.now(), infos['uamInitCustom'], infos['customCfg'])
    iheaders = {
            'Cookie': "hello1=" + cfg['id_userName']+ "; hello2=false; " + cookie,
            'Connection': 'keep-alive',
            'Referer': urls['tmpl:online'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg']),
            }
    return 'GET', url, iheaders, None

def online_heartBeat(cookie, infos):
    url = urls['tmpl:online_heartBeat'] % (infos['portalLink'], infos['uamInitCustom'])
    iheaders = {
            'Cookie': cookie,
            'Connection': 'keep-alive',
            'Referer': urls['tmpl:online'] % (infos['portalLink'], infos['uamInitCustom'], infos['customCfg']),
            }
    return 'GET', url, iheaders, None

def doHeartBeat(cookie, infos):
    url = urls['doHeartBeat']
    iheaders = {
        'Cookie': cookie,
        'Connection': 'keep-alive',
        'Referer': urls['tmpl:online_heartBeat'] % (infos['portalLink'], infos['uamInitCustom']),
        }
    userdata = "userip=&basip=&userDevPort=%s&userStatus=%s&serialNo=%s&language=%s&e_d=&t=hb" % (quote(infos['userDevPort']), infos['userStatus'], infos['serialNo'], infos['clientLanguage'])
    return 'POST', url, iheaders, userdata

def sendRequest(connection, cookie, infos, func):
    dp(func.__name__)
    method, url, headers, body = func(cookie, infos)
    headers.update(cfg['headers'])
    headers['Cookie'] = cookie
    connection.request(method, url, headers=headers, body=body)
    with connection.getresponse() as response:
        data = response.read()
    return data


def logout(cookie, headers, infos):
    dp("logout")
    connection = HTTPConnection(cfg['host'], cfg['port'])
    if DEBUG > 2:
        connection.set_debuglevel(1)
    def logout_stage1(cookie, infos):
        url = urls['tmpl:logout'] % (infos['portalLink'], infos['customCfg'], infos['uamInitCustom'])
        iheaders = {
                'Cookie': "hello1=" + cfg['id_userName']+ "; hello2=false; " + cookie,
                'Connection': 'keep-alive',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Referer': urls['tmpl:online_showTimer'] % (infos['portalLink'], utils.now(), infos['uamInitCustom'], infos['customCfg']),
                }
        return 'POST', url, iheaders, None
    def logout_stage2(cookie, infos):
        url = urls['tmpl:pws:lo'] % (infos['clientLanguage'], utils.now())
        iheaders = {
                'Cookie': cookie,
                'Connection': 'keep-alive',
                'X-Requested-With': 'XMLHttpRequest',
                'Referer': urls['tmpl:logout'] % (infos['portalLink'], infos['customCfg'], infos['uamInitCustom']),
                }
        return 'GET', url, iheaders, None
    sendRequest(connection, cookie, infos, logout_stage1)
    sendRequest(connection, cookie, infos, logout_stage2)

def main():
    lock = Lock()
    infos = {}
    def exit_auth(signum, frame):
        with lock:
            logout(cookie, cfg['headers'], infos)
            exit()
    signal(SIGINT, exit_auth)
    dp("creating connection")
    connection = HTTPConnection(cfg['host'], cfg['port'])
    if DEBUG > 2:
        connection.set_debuglevel(1)
    with lock:
        cookie = getCookie(connection, urls, cfg['headers'])
        cookie_info = utils.decode(cookie[7:])
        dp1(" cookie: %s" % cookie_info)
        if cookie_info['errorNumber'] != '1':
            raise ReturnedException(cookie_info)
        for info in ['clientLanguage', 'iNodePwdNeedEncrypt', 'assignIpType',
                     'customCfg', 'byodServerIp', 'byodServerHttpPort', 'uamInitCustom']:
            infos[info] = cookie_info[info]
        refer = sendRequest(connection, cookie, infos, login).decode('utf8')
        refer_info = utils.decode(refer)
        dp1(" refer: %s" % refer_info)
        if refer_info['errorNumber'] != '1':
            raise ReturnedException(refer_info)
        for info in ['userDevPort', 'userStatus', 'serialNo', 'clientLanguage', 'portalLink']:
            infos[info] = refer_info[info]
    with lock:
        sendRequest(connection, cookie, infos, afterLogin)
        sendRequest(connection, cookie, infos, openPage__online)
        sendRequest(connection, cookie, infos, listenClose)
        sendRequest(connection, cookie, infos, online_showTimer)
        #del connection
        connection = HTTPConnection(cfg['host'], cfg['port'])
        if DEBUG > 2:
            connection.set_debuglevel(1)
        sendRequest(connection, cookie, infos, online_heartBeat)
    cs = []
    while True:
        sleep(600) #sleep ~600s (1-603)
        connection = HTTPConnection(cfg['host'], cfg['port'])
        if DEBUG > 2:
            connection.set_debuglevel(1)
        with lock:
            sendRequest(connection, cookie, infos, doHeartBeat)
        cs.append(connection)
    with lock:
        logout(cookie, cfg['headers'], infos)

if __name__ == '__main__':
    try:
        main()
    except ReturnedException as e:
        print(e.info[e.info['e_d']])

